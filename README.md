# Flächenampel GFZ Converter

Dieses Modul dient dazu für Bebauungspläne mittlere GFZ-Werte aus einer bestehenden 
XPlanung-Datenbank zu berechnen

## Dependencies

### Python Dependencies

- Geopandas

### Other Dependencies

- Dieses setzt eine postgres-Datenbank mit dem xplanungs-schema voraus.

## Installation

Just follow the usual install process via pip. 

```bash
git clone (thisrepo)
cd thisreposdirectory
python -m pip install . #potentially use -e flag becaus this is work in progress
```