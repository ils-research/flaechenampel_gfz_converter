from setuptools import setup, find_packages

setup(
    name='flaechenampel_gfz_converter',
    version='0.0.1',
    install_requires=[
        'geopandas',
        'psycopg2',
        'GeoAlchemy2',
        'matplotlib',
        'folium'
    ],
    packages=find_packages(
        where='flaechenampel_gfz_converter'
    )
)