import os
# import datetime
import warnings
warnings.filterwarnings('ignore', category=UserWarning, module='openpyxl')
# import traceback
# import sys
import logging
# import linecache
# import inspect

from sqlalchemy import create_engine, schema, inspect as sql_inspect, text
# from geoalchemy2 import Geometry, WKTElement
import numpy as np
import pandas as pd
import folium
# import fiona
import geopandas as gpd
from .lib_external import slugify

import pkg_resources

try:
    dist = pkg_resources.get_distribution('IPython')
    from IPython.display import display
except pkg_resources.DistributionNotFound:
    pass

import inspect
import logging

class fa_gfz_convert(object):
    
    def __init__(self, redacted=False):
        self.redacted=redacted
        self.postgres_config = {}
        self.engine = None
        self.inspector = None
        self.map_root=''
        self.loglevel = logging.DEBUG
        self.moeglich = [
            "BMZ",
            "BMZmax",
            "BMZmin",
            "GFAntGewerbe",
            "GFAntWohnen",
            "GFGewerbe",
            "GFWohnen",
            "GFZ",
            "GFZmax",
            "GFZmin",
            "GR",
            "GRZ",
            "GRZmax",
            "GRZmin",
            "GRmax",
            "GRmin",
            "hMax",
            "hMin",
            "hZwingend",
            "h",
            "Z",
            "ZWohn",
            "Z_Ausn",
            "Z_Dach",
            "Z_Staffel",
            "Zmax",
            "Zmin",
            "Zzwingend",
            "allgArtDerBaulNutzung",
            "anz_zweckbestimmung",
            "besondereArtDerBaulNutzung",
            "geschossMax",
            "geschossMin",
            "wohnnutzungEGStrasse",
            "zweckbestimmung1", ## generisch für alle QGIS-Views (enthält Nutzungen, sofern attributiert)
            "ebene", ## BP_StraßenVerkehrsFlaeche -*gnaaah*
        ]
        logging.basicConfig(
            level=self.loglevel,
            format=('%(levelname)s: '
                    '%(pathname)s:%(lineno)d:\t'
                    '%(message)s'
            )
        )
        self.logger = logging.getLogger('flaechenampel')
    
    def _set_pg_config(self, pg_config):
        self.postgres_config = {
            "server": pg_config.get('server', 'localhost'),
            "port": pg_config.get('port', '5432'),
            "user": pg_config.get('user','xp_user'),
            "password": pg_config.get('password', 'password'),
            "database": pg_config.get('database','xp_planung')
        }
    
    def setLogLevel(self, loglevel):
        self.loglevel=loglevel
        self.logger.setLevel(self.loglevel)
        
    def init_db(self, pg_config, map_root=''):
        self._set_pg_config(pg_config)
        self.engine = create_engine(
            'postgresql://'+self.postgres_config['user']+':'+self.postgres_config['password']+'@'+self.postgres_config['server']+':'+self.postgres_config['port']+'/'+self.postgres_config['database'], 
            echo=False
        )
        self.inspector= sql_inspect(self.engine)
        if map_root!='' and os.path.exists(map_root):
            self.map_root = map_root
        return self.engine;
    
    def get_gid(self, name):
        if self.engine is None:
            raise NotInitiatedError('')
        sql = """SELECT a.*, b.gid as bgid 
FROM \"BP_Basisobjekte\".\"BP_Plan\" a
LEFT JOIN \"BP_Basisobjekte\".\"BP_Bereich\" b
ON a.gid=b."gehoertZuPlan"
WHERE a.name='""" + name + "'"
        with self.engine.connect() as conn: 
            df = pd.read_sql(sql, conn)
            # display(df)
        bgid = df['bgid'].item()
        gid = df['gid'].item()
        self.logger.debug('GID + BGID: ' + str((gid, bgid)))
        return bgid
        
    def get_flaeche(self, name, plotsize=False):
        gid = self.get_gid(name)
        sql = "SELECT * FROM \"BP_Basisobjekte\".\"BP_Bereich\" WHERE gid="+str(gid)+""
        with self.engine.connect() as conn: 
            df = gpd.read_postgis(sql, conn, geom_col='geltungsbereich')
        size = df.geometry.area.sum()
        if plotsize:
            df.plot(figsize=plotsize)
        return size
    
    
    def filterbuilder(self, dataframe, filterword='grz'):
        df = dataframe
        if type(filterword)==list:
            # col_list = list(set(filterword).intersection(set(df.columns)))
            col_list = []
            for y in filterword:
                col_list += [x for x in df.columns if y in x] 
            #display(('list: ',col_list))
            try:
                return eval('df['+'|'.join(['df[\''+x+'\'].notna()' for x in [x for x in col_list]])+']')
            except SyntaxError:
                self.logger.error('Searching for ' + str(filterword) + '/' + str(col_list) + ' in ' + str(dataframe.columns))
                # print(traceback.print_exception(*sys.exc_info()))
                # display(dataframe)
                self.logger.debug('df['+'|'.join(['df[\''+x+'\'].notna()' for x in [x for x in col_list]])+']')
                return pd.DataFrame(columns=[], index=[])
        else:
            #display(('string: ', filterword))
            try:
                return eval('df['+'|'.join(['df[\''+x+'\'].notna()' for x in [x for x in df.columns if filterword in x]])+']')
            except SyntaxError:
                self.logger.error('Searching for ' + str(filterword)+ ' in ' + str(dataframe.columns))
                # print(traceback.print_exception(*sys.exc_info()))
                # display(dataframe)
                self.logger.error('df['+'|'.join(['df[\''+x+'\'].notna()' for x in [x for x in df.columns if filterword in x]])+']')
                return pd.DataFrame(columns=[], index=[])
        
    def get_flaechen_basisdaten_dfs(self, name, plotsize=False, debug_=False):
        ## get relevant table/schema-combinations
        
        schemas = [n for n in self.inspector.get_schema_names() if 'BP_' in n or 'XP_Sonstiges' in n]
        config = []
        for vt in ['table','view']:
            for s in schemas:
                if vt =='table':
                    tabularitem = self.inspector.get_table_names(schema=s)
                else:
                    tabularitem = self.inspector.get_view_names(schema=s)
                for t in tabularitem:
                    columns = [x['name'] for x in self.inspector.get_columns(t, schema=s)]
                    cols_found = [c for c in columns if c in self.moeglich]
                    if (len(cols_found)>0):
                        config.append(
                            {
                                'schema': s,
                                'tabularitem': t,
                                "cols_found": cols_found,
                                "vt": vt
                            }
                        )
                        if debug_:
                            self.logger.debug("-- relevant " + vt + " found: " + s + '.'+t+' - ' + ', '.join(cols_found))
        
        
        # display(config)
        
        ## get gid
        gid = self.get_gid(name)
        ## get geometries with gid
        
        df_dict = {}
        df_dict_views = {}
        sqls = []
        with self.engine.connect() as conn: 
            for s, t, cols, vt in [x.values() for x in config]: #only recent python versions keep order of dict keys
                if t=='XP_Hoehenangabe':
                    sql_sachdaten = """
                        SELECT a."XP_Objekt_gid" as gid, b.* 
                        FROM
                            "XP_Basisobjekte"."XP_Objekt_hoehenangabe" a 
                        RIGHT JOIN
                            "XP_Sonstiges"."XP_Hoehenangabe" b
                        ON a.hoehenangabe=b.id
                        WHERE "XP_Objekt_gid" IN (SELECT "XP_Objekt_gid" FROM "XP_Basisobjekte"."XP_Objekt_gehoertZuBereich" WHERE "gehoertZuBereich" IN (""" + str(gid) + """))
                    """
                else:
                    sql_sachdaten = """
                SELECT gid, """ +','.join(["\""+v+"\" as "+v for v in cols]) + """ 
                FROM 
                    \""""+s+"""\".\"""" + t + """\" 
                WHERE 
                    gid IN (SELECT "XP_Objekt_gid" FROM "XP_Basisobjekte"."XP_Objekt_gehoertZuBereich" WHERE "gehoertZuBereich" IN (""" + str(gid) + """))
                    """
                sql_geom_sach = """
            SELECT a.*, b.* FROM
            (SELECT * FROM \"BP_Basisobjekte\".\"BP_Flaechenobjekt\" WHERE 
                gid IN (SELECT "XP_Objekt_gid" FROM "XP_Basisobjekte"."XP_Objekt_gehoertZuBereich" WHERE "gehoertZuBereich" IN (""" + str(gid) + """))
            ) a
            RIGHT JOIN
            (""" + sql_sachdaten + """) b
            ON a.gid=b.gid
            WHERE a.flaechenschluss=TRUE
                    ;
                """
                # print(sql_geom_sach)
                sqls.append(sql_geom_sach)
                
                df = gpd.read_postgis(sql_geom_sach, conn, geom_col='position')
                df = df.dropna(axis=1, how='all')
                if (df.columns.size==0 or df.columns[-1]=='gid'):
                    continue
                # print(sql_geom_sach)
                df=df[df.iloc[:,0].isin(df.iloc[:,0].dropna())]
                cols=pd.Series(df.columns)
                for dup in df.columns[df.columns.duplicated(keep=False)]: 
                    cols[df.columns.get_loc(dup)] = ([
                        dup + '_' + str(d_idx) 
                        if d_idx != 0 
                        else dup 
                        for d_idx in range(df.columns.get_loc(dup).sum())
                    ])
                df.columns=cols
                df = df.set_index('gid')
                df = df.dropna(axis=1, how='all')
                if t=='XP_Hoehenangabe' and 'bezugspunkt' in df.columns:
                    # print(sql_geom_sach)
                    moeglich_h = ['h','hmin','hmax','hzwingend']
                    col_sel = df.columns.intersection(moeglich_h)
                    traufhoehe = df[df['bezugspunkt']==1000].rename(columns={old: 'trauf_'+old for old in moeglich_h}, errors='ignore')
                    firsthoehe = df[df['bezugspunkt']==2000].rename(columns={old: 'first_'+old for old in moeglich_h}, errors='ignore')
                    gebaeudehoehe = df[df['bezugspunkt']==6000].rename(columns={old: 'geb_'+old for old in moeglich_h}, errors='ignore')
                    wandhoehe = df[df['bezugspunkt']==6500].rename(columns={old: 'wand_'+old for old in moeglich_h}, errors='ignore')
                    
                    irrelevant = ('gid', 'gid_1', 'position', 'flaechenschluss', "abweichenderHoehenbezug","bezugspunkt",'id')
                    
                    try:
                        df_casc_concat = df_casc_concat.join(traufhoehe[traufhoehe.columns.difference(irrelevant)], how='outer', rsuffix='_'+t)
                    except Exception as e:
                        display(df.columns)
                        display(irrelevant)
                        raise e
                    df_casc_concat = df_casc_concat.join(firsthoehe[firsthoehe.columns.difference(irrelevant)], how='outer', rsuffix='_'+t)
                    df_casc_concat = df_casc_concat.join(gebaeudehoehe[gebaeudehoehe.columns.difference(irrelevant)], how='outer', rsuffix='_'+t)
                    df_casc_concat = df_casc_concat.join(wandhoehe[wandhoehe.columns.difference(irrelevant)], how='outer', rsuffix='_'+t)
                    # display(df_casc_concat)
                    # return df_casc_concat
                else:
                    if t !='BP_FestsetzungenBaugebiet':
                        df.columns = df.columns.map(
                            lambda col: str(col) + '_'+t 
                            if col not in ('gid', 'gid_1', 'position', 'flaechenschluss')
                            else str(col)
                        )
                    relevant_cols = [col for col in df.columns if col not in ('gid', 'gid_1', 'position', 'flaechenschluss')]
                    if len(relevant_cols)>0:
                        display((vt,s,t))
                        display(relevant_cols)
                        # display((df.columns.size, df.index.size))
                        pass
    
                    if (t=='BP_UeberbaubareGrundstuecksFlaeche'):
                        df = df.assign(area=df.geometry.area)
                        relevant_cols +=['area']
                    try:
                        df_casc_concat = df_casc_concat.join(df[relevant_cols], how='outer', rsuffix='_'+t)
                    except UnboundLocalError:
                        df_casc_concat = df[['position']+relevant_cols]
                        # display(df_casc_concat)
                    except Exception as e:
                        display(df.columns)
                        display(relevant_cols)
                        raise e
        try:
            return df_casc_concat.dropna(how='all', axis=1)
        except:
            # for x,y in zip(config, sqls):
            #     print(x)
            #     print(y)
            return gpd.GeoDataFrame()
    
    def gfzcalculator(self, df, plotsize=True, foliumout=False):
        # display(df.columns)
        
        # display(Markdown('### Done: Zeilen rausschmeißen wo gfz/gfzminmax schon festgelegt sind'))
        filtered_gfz = self.filterbuilder(df,'gfz')
        
        filtered_z = self.filterbuilder(df,['z','zmin','zmax','zzwingend'])
        filtered_grz = self.filterbuilder(df,'grz')
        missing_indexes = filtered_z.index.difference(filtered_grz.index)
        errors = df[df.index.isin(missing_indexes)]
        if len(missing_indexes)>0:
            self.logger.warning('#### Folgende Dateneinträge sind evtl. fehlerhaft: ')
            self.logger.warning(str(missing_indexes))
            # display(errors)
            if 'display' in locals() or 'display' in globals():
                
                if self.redacted: background={"tiles": "https://stamen-tiles.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg", "attribution":"Map tiles by Stamen Design, under CC BY 3.0. Data by OpenStreetMap, under CC BY SA."}
                else: background={"tiles": "https://tile.openstreetmap.org/${z}/${x}/${y}.png", "attribution":"Data by OpenStreetMap Contributors, under CC BY SA."}
                m = folium.Map(
                    tiles=background['tiles'],
                    attr=background['attribution'],
                    zoom_end=13
                )
                m = errors.explore(m=m)
                m.fit_bounds(m.get_bounds())
                display(m)
        nobuild = df[~df.index.isin(filtered_z.index) & ~df.index.isin(filtered_grz.index)]
        
        # if filtered_gfz.index.size==df.index.size: #means: no gfz values included
        #     newdf = df[
        #         ~df.index.isin(nobuild.index) & 
        #         ~df.index.isin(errors.index)
        #     ].copy()
        # else: #
        newdf = df[
            ~df.index.isin(nobuild.index) & 
            ~df.index.isin(errors.index) & 
            ~df.index.isin(filtered_gfz.index)
        ].copy()
        
        missing_cols = list(set([x.lower() for x in self.moeglich]).difference([x.lower() for x in newdf.columns]))
        # display(missing_cols)
        for col in missing_cols:
            exec('newdf[\''+col+'\'] = np.NaN')
        
        no_z=newdf[newdf['z'].isna()]
        has_z=newdf[newdf['z'].notna()]
        
        is_zzwingend= no_z[no_z['zzwingend'].notna()]
        
        is_zminmax=no_z[no_z['zmin'].notna()&no_z['zmax'].notna()]
        
        no_grz = newdf[newdf['grz'].isna()]
        
        has_grz = newdf[newdf['grz'].notna()]
        
        is_grzminmax = no_grz[no_grz['grzmin'].notna() & no_grz['grzmax'].notna()]
        
        # grz and z exist:
        grz_z = newdf[
            newdf.index.isin(has_z.index.intersection(has_grz.index))
        ].copy()
        grz_z['gfz'] = grz_z['grz']*grz_z['z']
        
        # grz and zzwingend exist:
        grz_zzwingend = newdf[
            newdf.index.isin(is_zzwingend.index.intersection(has_grz.index))
        ].copy()
        grz_zzwingend['gfz'] = grz_zzwingend['grz']*grz_zzwingend['zzwingend']
        
        # grz and zmin and zmax exist:
        grz_zminmax = newdf[newdf.index.isin(is_zminmax.index.intersection(has_grz.index))].copy()
        grz_zminmax['gfzmin'] = grz_zminmax['grz']*grz_zminmax['zmin']
        grz_zminmax['gfzmax'] = grz_zminmax['grz']*grz_zminmax['zmax']
    
        # grz_minmax and z exist:
        grzminmax_z = newdf[
            newdf.index.isin(has_z.index.intersection(is_grzminmax.index))
        ].copy()
        grzminmax_z['gfzmin'] = grzminmax_z['grzmin']*grzminmax_z['z']
        grzminmax_z['gfzmax'] = grzminmax_z['grzmax']*grzminmax_z['z']
        
        # grz_minmax and zzwingend exist:
        grzminmax_zzwingend = newdf[
            newdf.index.isin(is_zzwingend.index.intersection(is_grzminmax.index))
        ].copy()
        grzminmax_zzwingend['gfzmin'] = grzminmax_zzwingend['grzmin']*grzminmax_zzwingend['zzwingend']
        grzminmax_zzwingend['gfzmax'] = grzminmax_zzwingend['grzmax']*grzminmax_zzwingend['zzwingend']
        
        # grz_minmax and zmin and zmax exist:
        grzminmax_zminmax = newdf[newdf.index.isin(is_zminmax.index.intersection(is_grzminmax.index))].copy()
        grzminmax_zminmax['gfzmin'] = grzminmax_zminmax['grzmin']*grzminmax_zminmax['zmin']
        grzminmax_zminmax['gfzmax'] = grzminmax_zminmax['grzmax']*grzminmax_zminmax['zmax']
        
        tables = [
            grz_z,
            grz_zzwingend,
            grz_zminmax,
            grzminmax_z,
            grzminmax_zzwingend,
            grzminmax_zminmax,
            filtered_gfz
        ]
        gfz_ready = pd.concat(tables).sort_index(axis=0).dropna(how='all', axis=1)
        gfz_missing = df[~df.index.isin(gfz_ready.index)].dropna(how='all', axis=1)
        
        try:
            assumeZ = gfz_missing[gfz_missing['grz'].notna()].copy()
        except:
            pass
        else:
            ### specials on height only implementend if grz is set (no grz_min/max, no grz_zwingend)
            if assumeZ.index.size>0:
                try: 
                    z_assumed = assumeZ['trauf_h']%4
                    assumeZ['z']=z_assumed
                    assumeZ['gfz']=assumeZ['grz']*assumeZ['z']
                except: pass
                try: 
                    zmin_assumed = assumeZ['trauf_hmin']%4
                    assumeZ['zmin']=zmin_assumed
                    assumeZ['gfzmin']=assumeZ['grz']*assumeZ['zin']
                except: pass
                try: 
                    zmax_assumed = assumeZ['trauf_hmax']%4
                    assumeZ['zmax']=zmax_assumed
                    assumeZ['gfzmax']=assumeZ['grz']*assumeZ['zmax']
                except: pass
                try: 
                    zzwingend_assumed = assumeZ['trauf_hzwingend']%4
                    assumeZ['zzwingend']=zzwingend_assumed
                    assumeZ['gfz']=assumeZ['grz']*assumeZ['zzwingend']
                except: pass
                if assumeZ.columns.intersection(['gfz','gfzmin','gfzmax']).size==0:
                    assumeZ['gfz']=assumeZ['grz']*1 ## no height to be found, so assume it has only one store
            gfz_missing = gfz_missing[~gfz_missing.index.isin(assumeZ.index)]
        # display(gfz_missing)
        # display(assume1)
        
        try:
            fulldf = pd.concat([
                gfz_missing,
                gfz_ready,
                assumeZ
            ])#.dropna(how='all', axis=1)
        except:
            fulldf = pd.concat([
                gfz_missing,
                gfz_ready
            ])#.dropna(how='all', axis=1)
            
        if plotsize:
            self.logger.info('#### Daten werden in Folium angezeigt:')
            if self.redacted: background={"tiles": "https://stamen-tiles.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg", "attribution":"Map tiles by Stamen Design, under CC BY 3.0. Data by OpenStreetMap, under CC BY SA."}
            else: background={"tiles": "https://tile.openstreetmap.org/${z}/${x}/${y}.png", "attribution":"Data by OpenStreetMap Contributors, under CC BY SA."}
            m = folium.Map(
                tiles=background['tiles'],
                attr=background['attribution'],
                zoom_end=13
            )
        
            try: 
                m = assumeZ.dropna(how='all', axis=1).explore(m=m, color='orange', name='assumeZ')
            except:  
                pass
            
            try: 
                m = grz_z.dropna(how='all', axis=1).explore(m=m, color='blue', name='grz_z')
            except:  
                pass
    
            try: 
                m = grz_zzwingend.dropna(how='all', axis=1).explore(m=m, color='cyan', name='grz_zzwingend')
            except:  
                pass
    
            try: 
                m = grz_zminmax.dropna(how='all', axis=1).explore(m=m, color='yellow', name='grz_zminmax')
            except:  
                pass
    
            try: 
                m = grzminmax_z.dropna(how='all', axis=1).explore(m=m, color='green', name='grzminmax_z')
            except:  
                pass
    
            try: 
                m = grzminmax_zzwingend.dropna(how='all', axis=1).explore(m=m, color='green', name='grzminmax_zzwingend')
            except:  
                pass
            
            try: 
                m = grzminmax_zminmax.dropna(how='all', axis=1).explore(m=m, color='green', name='grzminmax_zminmax')
            except:  
                pass
            
            try: 
                m = filtered_gfz.dropna(how='all', axis=1).explore(m=m, color='purple', name='filtered_gfz')
            except:  
                pass
            
            try: 
                m = gfz_missing.dropna(how='all', axis=1).explore(m=m, color='grey', name='gfz_missing')
            except:  
                pass
            folium.LayerControl(collapsed=False).add_to(m)
            m.fit_bounds(m.get_bounds())
            if "display" in locals() or 'display' in globals():
                display(m)
            if foliumout:
                if (self.redacted): self.logger.info('Folium Map saved on drive.')
                else: self.logger.info('Folium Map saved under ' + str(foliumout))
                m.save(foliumout)
        
        self.logger.info('#### Daten werden zurück gegeben')
        return fulldf
    
    
    def calculate_indicators(self, withgfz, foli = False, foliumout = False):
        results = {}
        gfzonly = withgfz[withgfz['gfz'].notna()].copy()
        try:
            gfzminmax = withgfz[withgfz['gfzmin'].notna() & withgfz['gfzmax'].notna()]
        except:
            gfzminmax = gpd.GeoDataFrame()
        if foli:
            if self.redacted: background={"tiles": "https://stamen-tiles.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg", "attribution":"Map tiles by Stamen Design, under CC BY 3.0. Data by OpenStreetMap, under CC BY SA."}
            else: background={"tiles": "https://tile.openstreetmap.org/${z}/${x}/${y}.png", "attribution":"Data by OpenStreetMap Contributors, under CC BY SA."}
            m = folium.Map(
                tiles=background['tiles'],
                attr=background['attribution'],
                zoom_end=13
            )
        
            
        indicator = 'geltungsbereich'
        try:
            results[indicator]= withgfz.geometry.area.sum()
            if foli:
                m = withgfz.assign(
                    dissolve='ja'
                ).dissolve(by='dissolve').reset_index().drop(columns='dissolve').explore(m=m, color='grey', name=indicator)
        except Exception as e:
            results[indicator] = np.NaN
            self.logger.error('Error while calculating ' + indicator + ' | ' + str(e))
            raise e
        
        
        indicator = 'gewerbe'
        try:
            gfzonly_gewerbe = withgfz[
                (withgfz['allgartderbaulnutzung_BP_BaugebietsTeilFlaeche']==2000) |
                (withgfz['allgartderbaulnutzung_BP_BaugebietsTeilFlaeche']==3000)
            ]
            gfzonly_gewerbe_gfz = gfzonly_gewerbe[gfzonly_gewerbe['gfz'].notna()]
            try:
                gfzonly_gewerbe_gfzminmax = gfzonly_gewerbe[gfzonly_gewerbe['gfzmin'].notna() & gfzonly_gewerbe['gfzmax'].notna()]
            except:
                gfzonly_gewerbe_gfzminmax = gpd.GeoDataFrame()
            df = gfzonly_gewerbe = pd.concat([
                gfzonly_gewerbe_gfz,
                gfzonly_gewerbe_gfzminmax
            ])
        except Exception as e:
            raise e
        else:
            results[indicator] = df.geometry.area.sum()
            if foli and df.index.size>0:
                m = df.explore(m=m, color='dimgrey', name=indicator)
        
        indicator = 'gfz_weighted_gewerbe'
        try:
            try: 
                gfz = (gfzonly_gewerbe_gfz.geometry.area*gfzonly_gewerbe_gfz['gfz']).sum()/gfzonly_gewerbe_gfz.geometry.area.sum()
                gfz_relative = gfz*gfzonly_gewerbe_gfz.geometry.area.sum()
                if np.isnan(gfz):
                    gfz = gfz_relative = 0
            except: 
                gfz = gfz_relative = 0
            try: 
                gfz_min = (
                    gfzonly_gewerbe_gfzminmax.geometry.area*gfzonly_gewerbe_gfzminmax['gfzmin']
                ).sum()/gfzonly_gewerbe_gfzminmax.geometry.area.sum()
                gfz_min_relative = gfz_min*gfzonly_gewerbe_gfzminmax.geometry.area.sum()
                if np.isnan(gfz_min):
                    gfz_min = gfz_min_relative = 0
            except: 
                gfz_min = gfz_min_relative = 0
            try: 
                gfz_max = (
                    gfzonly_gewerbe_gfzminmax.geometry.area*gfzonly_gewerbe_gfzminmax['gfzmax']
                ).sum()/gfzonly_gewerbe_gfzminmax.geometry.area.sum()
                gfz_max_relative = gfz_max*gfzonly_gewerbe_gfzminmax.geometry.area.sum()
                if np.isnan(gfz_max):
                    gfz_max = gfz_max_relative = 0
            except: 
                gfz_max = gfz_max_relative = 0
        except Exception as e:
            raise e
        else:
            relation_area = gfzonly_gewerbe.geometry.area.sum()
            if relation_area>0:
                results[indicator]= {
                    "gfzmin": (gfz_relative + gfz_min_relative)/relation_area,
                    "gfzmax": (gfz_relative + gfz_max_relative)/relation_area
                }
            else:
                results[indicator]= {
                    "gfzmin": 0,
                    "gfzmax": 0
                }
                
        indicator = 'wohnen'
        try:
            gfzonly_wohnen = withgfz[withgfz['allgartderbaulnutzung_BP_BaugebietsTeilFlaeche']==1000]
            gfzonly_wohnen_gfz = gfzonly_wohnen[gfzonly_wohnen['gfz'].notna()]
            try:
                gfzonly_wohnen_gfzminmax = gfzonly_wohnen[gfzonly_wohnen['gfzmin'].notna() & gfzonly_wohnen['gfzmax'].notna()]
            except:
                gfzonly_wohnen_gfzminmax = gpd.GeoDataFrame()
            df = gfzonly_wohnen = pd.concat([
                gfzonly_wohnen_gfz,
                gfzonly_wohnen_gfzminmax
            ])
        except Exception as e:
            raise e
        else:
            results[indicator] = df.geometry.area.sum()
            if foli and df.index.size>0:
                m = df.explore(m=m, color='maroon', name=indicator)
        
        indicator = 'gfz_weighted_wohnen'
        try:
            try: 
                gfz = (gfzonly_wohnen_gfz.geometry.area*gfzonly_wohnen_gfz['gfz']).sum()/gfzonly_wohnen_gfz.geometry.area.sum()
                gfz_relative = gfz*gfzonly_wohnen_gfz.geometry.area.sum()
                if np.isnan(gfz):
                    gfz = gfz_relative = 0
            except Exception as e: 
                gfz = gfz_relative = 0
                raise e
            try: 
                gfz_min = (gfzonly_wohnen_gfzminmax.geometry.area*gfzonly_wohnen_gfzminmax['gfzmin']).sum()/gfzonly_wohnen_gfzminmax.geometry.area.sum()
                gfz_min_relative = gfz_min*gfzonly_wohnen_gfzminmax.geometry.area.sum()
                if np.isnan(gfz_min):
                    gfz_min = gfz_min_relative = 0
            except Exception as e: 
                gfz = gfz_min_relative = 0
            try: 
                gfz_max = (gfzonly_wohnen_gfzminmax.geometry.area*gfzonly_wohnen_gfzminmax['gfzmax']).sum()/gfzonly_wohnen_gfzminmax.geometry.area.sum()
                gfz_max_relative = gfz_max*gfzonly_wohnen_gfzminmax.geometry.area.sum()
                if np.isnan(gfz_max):
                    gfz_min = gfz_max_relative = 0
            except Exception as e:
                gfz = gfz_max_relative = 0
            relation_area = gfzonly_wohnen.geometry.area.sum()
            if relation_area>0:
                results[indicator]= {
                    "gfzmin": (gfz_relative + gfz_min_relative)/relation_area,
                    "gfzmax": (gfz_relative + gfz_max_relative)/relation_area
                }
            else:
                results[indicator]= {
                    "gfzmin": 0,
                    "gfzmax": 0
                }
        except Exception as e:
            results[indicator] = np.NaN
            self.logger.error('Error while calculating ' + indicator + ' | ' + str(e))
            raise e
        
        
        indicator = 'bgf_wohnen'
        try:
            results[indicator] = (gfzonly_wohnen.geometry.area*gfzonly_wohnen['gfz']).sum()
        except Exception as e:
            results[indicator] = np.NaN
            self.logger.error('Error while calculating ' + indicator + ' | ' + str(e))
            raise e
        
        indicator = 'bgf_gewerbe'
        try:
            results[indicator] = (gfzonly_gewerbe.geometry.area*gfzonly_gewerbe['gfz']).sum()
        except Exception as e:
            results[indicator] = np.NaN
            self.logger.error('Error while calculating ' + indicator + ' | ' + str(e))
            raise e
        
        indicator = 'gruenflaechen'
        layernames = [
            'zweckbestimmung1_BP_GruenFlaeche_qv',
            'anz_zweckbestimmung_BP_GruenFlaeche_qv'
        ]
        included = withgfz.columns.intersection(layernames)
        included_seriesses = []
        for inc in included:
            included_seriesses.append('withgfz["'+inc+'"].notna()')
        if len(included_seriesses)>0:
            df = withgfz[
                eval(' | '.join([two for one, two in zip(included, included_seriesses)]))
            ]
            results[indicator] = df.geometry.area.sum()
        else:
            df = gpd.GeoDataFrame()
            results[indicator] = 0
        if foli and df.index.size>0:
            m = df.explore(m=m, color='green', name=indicator)
        
        indicator = 'verkehrsflaechen'
        layernames = [
            'ebene_BP_StrassenVerkehrsFlaeche_qv',
            'zweckbestimmung1_BP_VerkehrsFlaecheBesondererZweckbestimmungFlaeche_qv',
            'anz_zweckbestimmung_BP_VerkehrsFlaecheBesondererZweckbestimmungFlaeche_qv'
        ]
        included = withgfz.columns.intersection(layernames)
        included_seriesses = []
        for inc in included:
            if inc=='zweckbestimmung1_BP_VerkehrsFlaecheBesondererZweckbestimmungFlaeche_qv':
                included_seriesses.append('withgfz["'+inc+'"]==1200')
            else:
                included_seriesses.append('withgfz["'+inc+'"].notna()')
        if len(included_seriesses)>0:
            df = withgfz[
                eval(' | '.join([two for one, two in zip(included, included_seriesses)]))
            ]
            results[indicator] = df.geometry.area.sum()
        else:
            df = gpd.GeoDataFrame()
            results[indicator] = 0
        if foli and df.index.size>0:
            m = df.explore(m=m, color='yellow', name=indicator)
        
        
        
        indicator = 'flaeche_nur_G_W_Gruen_Verkehr'
        try:
            relevant_areas = set(results.keys()).intersection(['wohnen','gewerbe','gruenflaechen','verkehrsflaechen'])
            flaeche = 0
            for x in relevant_areas:
                if not np.isnan(results[x]):
                    flaeche+=results[x]
            results[indicator]= flaeche
        except Exception as e:
            results[indicator] = np.NaN
            self.logger.error('Error while calculating ' + indicator + ' | ' + str(e))
            raise e
        
        if foli:
            m.fit_bounds(m.get_bounds())
            m.add_child(folium.LayerControl( collapsed=False))
            if "display" in locals():
                display(m)
            if foliumout:
                if (self.redacted): self.logger.info('Folium Map saved on drive.')
                else: self.logger.info('Folium Map saved under ' + str(foliumout))
                m.save(foliumout)
        return results


    def run_full(self, plan, redacted=False):
        self.redacted=redacted
        bplanname = plan
        gid = self.get_gid(bplanname)
        size = self.get_flaeche(bplanname)
        basisdf = self.get_flaechen_basisdaten_dfs(bplanname, plotsize=(2,2))
        try:
            basisdf_geom = basisdf[basisdf['position'].notna()]
        except Exception as e:
            basisdf_geom = gpd.GeoDataFrame()
            raise e
        else:
            if self.map_root:
                withgfz = self.gfzcalculator(
                    basisdf_geom, 
                    foliumout=os.path.join(
                        self.map_root,
                        slugify(plan) + ".html"
                    )
                )
                indicators  = self.calculate_indicators(withgfz, foli=True, 
                    foliumout=os.path.join(
                        self.map_root,
                        slugify(plan) + "_indicator.html"
                    ))
            else:
                withgfz = self.gfzcalculator(
                    basisdf_geom
                )
                indicators  = self.calculate_indicators(
                    withgfz, 
                    foli=False
                )
            return indicators
        # return {'error': 'fatal'}
        
    def get_plaene(self):
        sql = "SELECT * FROM \"BP_Basisobjekte\".\"BP_Plan\""
        with self.engine.connect() as conn: 
            df = pd.read_sql(sql, conn)
            # display(df)
        return df
        # gid = df['gid'].item()
        # return gid
        
class NotInitiatedError(Exception):
    def __init__(self, message, errors):            
        # Call the base class constructor with the parameters it needs
        super().__init__('DB-Connection not initialized yet: Please run init_db before using any other method.' + message)


# alleplaene_markdown = ['' + str(x) for x in list(self.get_plaene()['name'].values)]
# for p in alleplaene_markdown:
#     display(Markdown(p))
# alleplaene = list(get_plaene()['name'].values)